﻿using System;
using EventsLibrary;

namespace TestApp
{
    class Program
    {
        static void  Main(string[] args)
        {
            string[] filenames = {"Паспорт.jpg", "Заявление.txt", "Фото.jpg"};

            DocumentsReceiver receiver = new DocumentsReceiver(filenames);
            receiver.Start(15, "D:\\temp\\Test");
            receiver.DocumentsReady += Receiver_DocumentsReady;
            receiver.TimeOut += Receiver_TimeOut;
            Console.ReadLine();
        }

        private static void Receiver_TimeOut()
        {
            Console.WriteLine("Timeout!");
        }

        private static void Receiver_DocumentsReady(object sender, System.IO.FileSystemEventArgs eventArgs)
        {
            Console.WriteLine($"Files in {eventArgs.FullPath} is ready");
        }
    }
}
