﻿using System;
using System.IO;
using System.Timers;
using System.Collections;
using System.Collections.Generic;

namespace EventsLibrary
{
    public class DocumentsReceiver
    {
        Timer timer;
        readonly string[] FileNames;
        public event DocumentsReadyHandler DocumentsReady;
        public event Action TimeOut;

        public delegate void DocumentsReadyHandler(object sender, FileSystemEventArgs eventArgs);
      //  public delegate void TimerOutHandler(object sender, ElapsedEventArgs eventArgs);


        private FileSystemWatcher fileSystemWatcher;
        public DocumentsReceiver(string[] filenames)
        {
            FileNames = filenames;
            fileSystemWatcher = new FileSystemWatcher();
            
            timer = new Timer();
           
        }

        public void Start(double waitingInterval, string watchingDirectory)
        {
            fileSystemWatcher.Path = watchingDirectory;
            fileSystemWatcher.EnableRaisingEvents = true;
            timer.Interval = waitingInterval * 1000;
            timer.Start();
            timer.Elapsed += OnTimeOut;
            fileSystemWatcher.Changed += DirectoryChanged;
            fileSystemWatcher.Created += DirectoryChanged;
            fileSystemWatcher.Renamed += DirectoryChanged;
           // DocumentsReady += DoSomethingWithDocuments;
        }

        public void DirectoryChanged(object sender, FileSystemEventArgs eventArgs)
        {
            if (CheckFiles())
            {
                DocumentsReady?.Invoke(sender, eventArgs);
                timer.Stop();
            }
        }

        public void  OnTimeOut(object sender, ElapsedEventArgs args)
        {
            TimeOut?.Invoke();
            timer.Elapsed -= OnTimeOut;
            fileSystemWatcher.Changed -= DirectoryChanged;
            fileSystemWatcher.Created -= DirectoryChanged;
            fileSystemWatcher.Renamed -= DirectoryChanged;
        }

        private bool CheckFiles()
        {
            var Path = fileSystemWatcher.Path;           
            var CurrentFilesList = System.IO.Directory.GetFiles(Path);
            int count=0;
            foreach (string EtalonFile in FileNames)
            {
                var currentEthalonFile = Path +"\\" + EtalonFile;
                foreach (string CurFile in CurrentFilesList)
                {
                    if (String.Compare(CurFile, currentEthalonFile) == 0)
                        count++;
                }
            }
            if (count == 3)
                return true;
            else return false;
        }
        
  



    }
}
